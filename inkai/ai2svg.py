#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) 2022 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

"""Conversion of AI (internal object storage) to SVG"""

import sys
from pathlib import Path
from typing import Optional, ByteString
import zlib
from io import BytesIO

from PyPDF2 import PdfReader
from PyPDF2.constants import PageAttributes
import zstandard

import inkex
from inkex.localization import inkex_gettext as _

# This bit of import fiddling is taken from
# https://stackoverflow.com/a/28154841/3298143, and it is necessary
# so the script can run both with -m (needed for pypi) as well as
# directly (needed for being an extensions submodule)
if __name__ == "__main__" and (__package__ is None or __package__ == ""):
    file = Path(__file__).resolve()
    parent, top = file.parent, file.parents[1]

    sys.path.append(str(top))
    try:
        sys.path.remove(str(parent))
    except ValueError:  # Already removed
        pass

    __package__ = "inkai"


def extract_ai_privatedata(content: ByteString) -> Optional[str]:
    """Parse the contents depending on the AI version.
    AI3-8: AI-Postscript (nothing to do)
    AI9+: PDF with embedded AIPrivateData* dict entries. These are concatenated
          across all pages. The contents always start with a header and a thumbnail.
          What happens afterwards differs from version to version:
      - AI9-10: /FlateDecode'd data after the thumbnail
      - AI CS - CS6, CC Legacy: %AI12_CompressedData followed by zlib-deflated content
      - AI CS2020: %AI24_ZStandard_Data followed by zst-deflated content
        (probably with pre-trained compression dictionary)

    If the file can be parsed, the content is returned as str.
    Otherwise, None is returned."""
    if content.startswith(b"%PDF-"):
        reader = PdfReader(BytesIO(content))

        # Concatenate all AIPrivateData blocks.
        number_of_pages = len(reader.pages)
        for npage in range(number_of_pages):
            page = reader.pages[npage]
            piece_info = page[PageAttributes.PIECE_INFO]
            ilpr = piece_info["/Illustrator"]["/Private"]

            blocks = ilpr["/NumBlock"]

            result = b""
            keys = [f"/AIPrivateData{b}" for b in range(1, int(blocks) + 1)]
            for key in keys:
                data = ilpr[key]
                result += data._data

        decompressed = b""
        if b"%AI12_CompressedData" in result:
            result = result.split(b"%%EndData")
            result[1] = result[1].lstrip()
            archive = result[1][len("%AI12_CompressedData") :]
            assert chr(archive[0]) == "x"
            decompressed = zlib.decompress(archive)
        elif b"%AI24_ZStandard_Data" in result:
            archive = result.split(b"%AI24_ZStandard_Data")
            assert chr(archive[1][0]) == "("
            # We have to specify the max output size. This is memory that
            # will definitely be allocated, so don't overdo it. The compression
            # ratio of zst is approx. 2.9 on typical text files, so this
            # seems like a reasonable guess.
            try:
                decompressed = zstandard.decompress(
                    archive[1], max_output_size=len(archive[1]) * 10
                )
            except zstandard.ZstdError:
                print("Unable to decompress data. Try increasing the output size")
                return ""
        else:
            result = result.split(b"%%EndData")
            result[1] = result[1].lstrip()
            try:
                decompressed = zlib.decompress(result[1])
            except zlib.error:
                print("Unable to decompress zlib archive.")
                return ""
        return decompressed

    if content.startswith(b"%!PS-Adobe-3.0"):
        # very old illustrator file, already decompressed
        return content
    return ""


class AIInput(inkex.InputExtension):
    """Load AI File"""

    def __init__(self) -> None:
        super().__init__()
        self.raw = b""

    def add_arguments(self, pars):
        pass

    def load(self, stream):
        """Load the file. If we are unable to extract the AI data, return the unaltered
        PDF file."""
        raw = stream.read()
        parsed = extract_ai_privatedata(raw)
        if parsed is None:
            inkex.errormsg(_("Unable to extract AI data. Falling back to PDF."))
            return parsed
        else:
            return self.parse_ai_data(parsed)

    def parse_ai_data(self, parsed: str) -> inkex.SvgDocumentElement:
        """Translate the contents of the decoded AI private data to SVG."""
        doc = self.get_template(width=480, height=360, unit="mm")
        svg = doc.getroot()
        tel = inkex.TextElement()
        tel.text = (
            "Hello World! AI file read successfully, created with version "
            + parsed.splitlines()[1].replace(b"%%Creator: ", b"").decode("utf-8")
        )
        svg.add(tel)
        return doc


if __name__ == "__main__":
    AIInput().run()
