import context

import inkai.ai2svg as ai2svg

from inkex.tester import TestCase

"""Test that we can extract the private data of different documents"""


class DataExtractionTest(TestCase):
    """Test that we can extract the private data of different documents"""

    def test_ai_versions(self):
        """Test different AI version"""
        cases = (
            ("simple_v3.ai", "3.2"),
            ("simple_v3_jp.ai", "3.2"),
            ("simple_v8.ai", "8.0"),
            ("simple_v9.ai", "9.0"),
            ("simple_v10.ai", "10.0"),
            ("simple_cs.ai", "11.0"),
            ("simple_cs2.ai", "12.0"),
            ("simple_cs3.ai", "13.0"),
            ("simple_cs4.ai", "14.0"),
            ("simple_cs5.ai", "15.0"),
            ("simple_cs6.ai", "16.0"),
            ("simple_cc_legacy.ai", "17.0"),
            ("simple_v2020.ai", "24.0"),
            ("simple_v2020_no_pdf_data.ai", "24.0"),
        )

        for filename, version in cases:
            file = self.data_file("ai", filename)
            with open(file, "rb") as f:
                content = f.read()
            result = ai2svg.extract_ai_privatedata(content).splitlines()
            self.assertEqual(result[0].strip(), b"%!PS-Adobe-3.0")
            self.assertIn(
                result[1].strip(),
                [
                    f"%%Creator: Adobe Illustrator(R) {version}".encode("utf-8"),
                    f"%%Creator: Adobe Illustrator(TM) {version}".encode("utf-8"),
                ],
            )
